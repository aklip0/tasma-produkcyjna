#pragma once
#include "stdafx.h"
#include "globals.hpp"
#include "module.hpp"
#include "loader.hpp"
#include "message.hpp"

class obj_manager;


enum obj_class_type
{
	STATIC_OBJ = 1, // nie moze to byc zero, bo 0 jest po inicjalizacji
	DYNAMI_OBJ
};

istream& operator>>(istream&, obj_class_type&);

class obj
{
public:
	obj(obj_class_type);
	~obj();


	void update(render_wnd&, int dt);
	void init(const obj*);
	void delete_ptr();

	virtual bool message(); // todo
	std::string toString() const;
	istream& scan(istream&, const std::string filename=""); //laduje dane ze strumienia
	ostream& print(ostream&) const; // wrzuca dane do strumienia
	ostream& operator<<(ostream&) const; // wywoluje print
	istream& operator>>(istream&); // wywoluje scan

	//template<typename... args>
	//virtual bool message(const message& msg, const module_type& target, args ...);


	friend class obj_manager;
	//friend class module;

protected:

	// protected laduje dane z pliku
	void init(const std::string obj_name);
	const obj_class_type _oct;
	std::vector<module*> _modules;
};

