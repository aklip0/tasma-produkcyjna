﻿#pragma once
#include "globals.hpp"



template<typename T>
class task;
class module;
class obj;
class manager;

///////////////////////////////////
////// TASK  //////////////////////
///////////////////////////////////

//interfejs szablony COMMAND
template<typename T>
class task
{
protected:
	// destination object
	T* _dst;
	time_t _deadtime;

	task();
	task(T* dst) : _dst(dst), _deadtime(clock()) {}

public:
	virtual void exec() = 0;
	time_t getDeadtime() { return _deadtime; }
};



///////////////////////////////////
////// MODULE  ////////////////////
///////////////////////////////////

enum module_type
{
	MODULE_ALL = -1,
	MODULE_NONE = 0, // wartosc domyslna
	MODULE_DRAWABLE = 1, // != 0, bo 0 jest wartoscia domyslna
	MODULE_PHYSICAL,
	MODULE_SCRIPTABLE,
	//ponizej sa moduly poszczegolnych rodzajow obiektow
	MODULE_TLOK,
	MODULE_FACTORY,
	MODULE_TERMINATOR = 103
};


// interfejs
class module
{
public:
	virtual ~module();

	// inrerfejs
	virtual void update(int dt) = 0;
	virtual void init(const module&);
	virtual void new_ptr(const std::string& name) = 0;
	virtual void delete_ptr() = 0;
	virtual std::string toString() const = 0;


	// IO operators
	virtual ostream& print(ostream&) const = 0;
	virtual istream& scan(istream&, const std::string& filename = "") = 0;
	friend ostream& operator<<(ostream&, const module&);
	friend istream& operator>>(istream&, module&);

	// own functions
	module_type type() const;
	bool isScriptable() const;
	module* clone() const;

	// static functions
	static void init();
	static module* create(const module_type&, obj* o = NULL);
	static module_type get_mod(const string&);
	static const string& get_mod(const module_type&);

protected:
	// private constructor!!
	// it can be created by module::create(string)
	module(module_type, bool scriptable = false);

private:
	// truly const value, because there is only get methode and private
	// perhaps only init method may overwrite _type
	module_type _type;
	bool _scriptable;

	// static fields
	static map<string, module_type> _get_mod_type;
};

istream& operator>>(istream& i, module_type&);




///////////////////////////////////
//////////// OBJ   ////////////////
///////////////////////////////////
class obj_manager;

typedef std::vector<module*> module_container;


enum obj_type
{
	STATIC_OBJ = 1, // nie moze to byc zero, bo 0 jest po inicjalizacji
	DYNAMIC_OBJ,
	TRIANGLE_OBJ,
	RECTANGLE_OBJ,
	TLOK_OBJ,
	PIEC_OBJ,
	KONIEC_OBJ
};

istream& operator>>(istream&, obj_type&);

class obj
{
public:
	obj(obj_type);
	~obj();


	void update(int dt);
	void init(const obj*);
	void delete_ptr();
	module* getModule(const module_type&);
	obj_type getType() const;


	std::string toString() const;
	istream& scan(istream&, const std::string filename = ""); //laduje dane ze strumienia
	ostream& print(ostream&) const; // wrzuca dane do strumienia
	ostream& operator<<(ostream&) const; // wywoluje print
	istream& operator>>(istream&); // wywoluje scan

	//template<typename... args>
	//virtual bool message(const message& msg, const module_type& target, args ...);


	friend class obj_manager;
	//friend class module;

protected:

	// protected laduje dane z pliku
	void init(const std::string obj_name);
	const obj_type _ot;
	module_container _modules;
};


///////////////////////////////////
////// MANAGER  ///////////////////
///////////////////////////////////

enum manager_type
{
	OBJ_MANAGER
};

class manager
{
public:
	manager(const manager_type& mt)						: _mt(mt) {}
	const manager_type& type()							{ return _mt; }

	virtual void update(int dt) = 0;
private:
	manager_type _mt;
};