#pragma once
#include "stdafx.h"

using namespace std;

class settings
{
public:
	settings() {}
	virtual ~settings() {}

public:
	wstring spritFile = L"data/sprite.png";

	string dataPath = "data/";
	string dataExtension = ".txt";
	wstring renderablePath = L"data/";
	wstring renderableExtension = L".png";
	string fileStart = "{";
	string fileEnd = "}";

	sizeI blockSize = sizeI(40, 40);
};

