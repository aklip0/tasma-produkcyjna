#include "stdafx.h"
#include "m_scriptable.hpp"


m_scriptable::m_scriptable(obj* o, const module_type& mt) : module(mt, true), _obj(o)
{
}


m_scriptable::~m_scriptable()
{
}



// inrerfejs
/*void m_scriptable::update(int dt)
{

}*/
void m_scriptable::init(const module&)
{

}

void m_scriptable::setSimulator(simulator* s)
{
	_sim = s;
}


void m_scriptable::new_ptr(const std::string& name)
{

}


void m_scriptable::delete_ptr()
{

}



void m_scriptable::collision(obj* o, sizeF inter)
{
	//todo: 
}




std::string m_scriptable::toString() const
{
	stringstream ss(get_mod(MODULE_SCRIPTABLE));
	print(ss);
	return ss.str();
}


// IO operators
ostream& m_scriptable::print(ostream& o) const
{
	loader::printStart(o, get_mod(module_type::MODULE_SCRIPTABLE));
	loader::print(o, type(), "type");
	loader::printEnd(o);
	return o;
}



istream& m_scriptable::scan(istream& i, const std::string& filename)
{
	module_type mt;

	loader::scanStart(i);
	loader::scan(i, mt, filename);

	// male sprawdzanie poprawnosci typow
	if (mt != type())
		throw wexception_t(G::text("badModuleType"), filename + to_string(i.tellg()) + " module_type:" + to_string(static_cast<int>(mt)) + "->" + to_string(static_cast<int>(type())));

	loader::scanEnd(i);
	return i;
}


void m_scriptable::position(pointF p)
{
}