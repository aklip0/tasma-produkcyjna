// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

//#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// STL
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <memory>
#include <map>
#include <stdexcept> // out_of_range from unordered map
#include <string>
#include <unordered_map> // text
#include <vector>

// MOJE 
#include "tools.hpp"
#include "string_cast.hpp"

// GDI
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;