#include "stdafx.h"
#include "m_terminator.hpp"
#include "simulator.hpp"

m_terminator::m_terminator(obj* o) : m_scriptable(o, MODULE_TERMINATOR)
{
}


m_terminator::~m_terminator()
{
}

void m_terminator::update(int dt)
{

}

void m_terminator::collision(obj* o, sizeF s)
{
	obj_manager* om;

	if (_sim == NULL)
		throw wexception_t(G::text("badModuleScriptable"), module::get_mod(module_type::MODULE_FACTORY));

	om = static_cast<obj_manager*>(_sim->getManager(manager_type::OBJ_MANAGER));
	om->addTask(new task_om_del_obj(om, o));
}


ostream& m_terminator::print(ostream& o) const
{
	loader::printStart(o, module::get_mod(module_type::MODULE_TERMINATOR));
	loader::print(o, type(), "type");
	loader::printEnd(o);
	return o;
}

istream& m_terminator::scan(istream& i, const string& filename)
{
	module_type mt;

	loader::scanStart(i, filename);
	loader::scan(i, mt, filename);

	if (mt != type())
		throw wexception_t(G::text("badModuleType"), filename + to_string(i.tellg()) + " module_type:" + to_string(static_cast<int>(mt)) + "->" + to_string(static_cast<int>(type())));
	
	loader::scanEnd(i, filename);
	return i;
}