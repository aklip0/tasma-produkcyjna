#pragma once
#include "globals.hpp"
#include "obj_manager.hpp"
#include "interfaces.hpp"

typedef std::vector<manager*> manager_container;

class simulator
{
public:
	simulator(HWND);
	~simulator();

	void start();
	void update(int dt);
	void draw(render_wnd* = NULL);

	manager* getManager(const manager_type&);
	manager_container::iterator& firstManager();
	bool nextManager(manager_container::iterator&);

	void set_render_wnd(render_wnd* p) { _wnd = p; }

private:
	render_wnd* _wnd;
	HDC _hdc;
	obj_manager* _objects;
	manager_container _managers;
};





////////////////////
/////// TASK ///////
////////////////////


class task_s_draw : task < simulator >
{
	render_wnd* _ptr;
public:
	task_s_draw(simulator* dst, render_wnd* wnd) : task(dst), _ptr(wnd) {}
	virtual void exec();
};