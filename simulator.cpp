﻿#include "stdafx.h"
#include "simulator.hpp"

#include "m_drawable.hpp"


simulator::simulator(HWND hwnd)
{
	_hdc = GetDC(hwnd);
	_wnd = new render_wnd(_hdc);

	if (!_wnd)
		throw wexception_t(L"ERROR with creating simluator render window", "");

	G::init();
}


simulator::~simulator()
{
	if (_objects)
		delete _objects;

	if (_wnd)
		delete _wnd;

}



void simulator::start()
{
	try
	{
		obj_manager* o_man = new obj_manager(this);
		_managers.push_back(o_man);
		_objects = o_man;
		task_om_load_map(o_man, "map0").exec();

	}
	catch (const wexception& e)
	{
		wstring msg = e.wwhat();
		MessageBox(NULL, (LPCWSTR)msg.c_str(), NULL, MB_OK|MB_ICONERROR);
	}
	catch (const exception& e)
	{
		string msg(e.what());
		MessageBoxA(NULL, msg.c_str(), NULL, MB_OK|MB_ICONERROR);
	}
}



void simulator::update(int dt)
{
	manager_container::iterator it;

	// w przypadku zaciecia nie bedzie przeskoku duzego
	dt = dt % 1000;

	for (it = _managers.begin(); it != _managers.end(); ++it)
		(*it)->update(dt);

}

void simulator::draw(render_wnd* rw)
{
	if (rw)
		task_s_draw(this, rw).exec();
	else if (_wnd)
		task_s_draw(this, _wnd).exec();
	else
		throw wexception_t(G::text("render_wndError"), "");
}

manager* simulator::getManager(const manager_type& mt)
{
	manager_container::iterator it = _managers.begin();

	while (it != _managers.end() && (*it)->type() != mt)
		++it;

	if ((*it)->type() == mt)
		return *it;
	else
		return NULL;
}

manager_container::iterator& simulator::firstManager()
{
	return _managers.begin();
}

bool simulator::nextManager(manager_container::iterator& it)
{
	if (it != _managers.end())
		++it;

	if (it == _managers.end())
		return false;
	else
		return true;
}





void task_s_draw::exec()
{
	manager* m;
	m = _dst->getManager(manager_type::OBJ_MANAGER);

	if (m != NULL)
	{
		task_om_draw(static_cast<obj_manager*>(m), _ptr).exec(); //todo: usun .exec()
	}
}