#include "stdafx.h"
#include "m_factory.hpp"
#include "simulator.hpp"

m_factory::m_factory(obj* o) : m_scriptable(o, MODULE_FACTORY)
{
}


m_factory::~m_factory()
{
}


void m_factory::update(int dt)
{
	obj_manager* om;
	_time += dt;

	if (_time >= _duration)
	{
		if (_sim == NULL)
			throw wexception_t(G::text("badModuleScriptable"), module::get_mod(module_type::MODULE_FACTORY));

		om = static_cast<obj_manager*>(_sim->getManager(manager_type::OBJ_MANAGER));
		
		if (rand() % 2)
			om->createObj("trojkat", _pos);
		else
			om->createObj("kwadrat", _pos);

		_time %= _duration;
	}
}


void m_factory::init(const module& m)
{
	const m_factory* mf;

	if (m.type() != type())
		throw wexception_t(G::text("badModule"), to_string(type()) + "<->" + to_string(m.type()));
	
	mf = static_cast<const m_factory*>(&m);

	_duration = mf->_duration;
}


ostream& m_factory::print(ostream& o) const
{
	loader::printStart(o, m_scriptable::get_mod(module_type::MODULE_FACTORY));
	loader::print(o, type(), "type");
	loader::print(o, _duration, "_duration");
	loader::printEnd(o);
	return o;
}

istream& m_factory::scan(istream& i, const string& filename)
{
	module_type mt;

	loader::scanStart(i, filename);
	loader::scan(i, mt, filename);

	if (mt != type())
		wexception_t(G::text("badModuleType"), filename + to_string(i.tellg()) + " module_type:" + to_string(static_cast<int>(mt)) + "->" + to_string(static_cast<int>(type())));

	loader::scan(i, _duration, filename);
	loader::scanEnd(i, filename);
	return i;
}

void m_factory::position(pointF p)
{
	_pos = p;
}