#pragma once

#include <codecvt> // konwersje stringow
//#include <vector>
#include <string>
//#include <cstring>
//#include <cwchar>
#include <cassert>

template<typename TO, typename FROM>
TO string_cast(FROM s);

template<typename TO>
TO string_cast(const std::string& src)
{
	typedef std::codecvt_utf8<wchar_t> convert_type;
	std::wstring_convert<convert_type, wchar_t> converter;

	return converter.from_bytes(src);
}


template<typename TO>
TO string_cast(std::wstring src)
{
	typedef std::codecvt_utf8<wchar_t> convert_type;
	std::wstring_convert<convert_type, wchar_t> converter;
	return converter.to_bytes(src);
}



//currently active code page (CP_ACP)
// CP_UTF8
/*template<typename Td>
Td string_cast(const char* pSource, unsigned int codePage = CP_ACP);

template<typename Td>
Td string_cast(const wchar_t* pSource, unsigned int codePage = 1200);

template<typename Td>
Td string_cast(const std::string& source, unsigned int codePage = CP_ACP);

template<typename Td>
Td string_cast(const std::wstring& source, unsigned int codePage = 1200);

template<>
std::string string_cast(const char* pSource, unsigned int codePage)
{
	assert(pSource != 0);
	return std::string(pSource);
}

template<>
std::wstring string_cast(const char* pSource, unsigned int codePage)
{
	assert(pSource != 0);
	std::size_t sourceLength = std::strlen(pSource);
	if (sourceLength == 0)
	{
		return std::wstring();
	}

	int length = ::MultiByteToWideChar(codePage, 0, pSource, sourceLength, NULL, 0);
	if (length == 0)
	{
		return std::wstring();
	}

	std::vector<wchar_t> buffer(length);
	::MultiByteToWideChar(codePage, 0, pSource, sourceLength, &buffer[0], length);

	return std::wstring(buffer.begin(), buffer.end());
}

template<>
std::string string_cast(const wchar_t* pSource, unsigned int codePage)
{
	assert(pSource != 0);
	size_t sourceLength = std::wcslen(pSource);
	if (sourceLength == 0)
	{
		return std::string();
	}

	int length = ::WideCharToMultiByte(codePage, 0, pSource, sourceLength, NULL, 0, NULL, NULL);
	if (length == 0)
	{
		return std::string();
	}

	std::vector<char> buffer(length);
	::WideCharToMultiByte(codePage, 0, pSource, sourceLength, &buffer[0], length, NULL, NULL);

	return std::string(buffer.begin(), buffer.end());
}

template<>
std::wstring string_cast(const wchar_t* pSource, unsigned int codePage)
{
	assert(pSource != 0);
	return std::wstring(pSource);
}

template<>
std::string string_cast(const std::string& source, unsigned int codePage)
{
	return source;
}

template<>
std::wstring string_cast(const std::string& source, unsigned int codePage)
{
	if (source.empty())
	{
		return std::wstring();
	}

	int length = ::MultiByteToWideChar(codePage, 0, source.data(), source.length(), NULL, 0);
	if (length == 0)
	{
		return std::wstring();
	}

	std::vector<wchar_t> buffer(length);
	::MultiByteToWideChar(codePage, 0, source.data(), source.length(), &buffer[0], length);

	return std::wstring(buffer.begin(), buffer.end());
}

template<>
std::string string_cast(const std::wstring& source, unsigned int codePage)
{
	if (source.empty())
	{
		return std::string();
	}

	int length = ::WideCharToMultiByte(codePage, 0, source.data(), source.length(), NULL, 0, NULL, NULL);
	if (length == 0)
	{
		return std::string();
	}

	std::vector<char> buffer(length);
	::WideCharToMultiByte(codePage, 0, source.data(), source.length(), &buffer[0], length, NULL, NULL);

	return std::string(buffer.begin(), buffer.end());
}

template<>
std::wstring string_cast(const std::wstring& source, unsigned int codePage)
{
	return source;
}

template<typename TO, typename FROM>
TO string_cast(FROM s);

template<typename TO>
TO string_cast(const std::string& src)
{
	int codePage = CP_ACP;
	int length = ::MultiByteToWideChar(codePage, 0, src.data(), src.length(), NULL, 0);
	if (length == 0)
	{
		return TO();
	}

	std::vector<wchar_t> buffer(length);
	::MultiByteToWideChar(codePage, 0, src.data(), src.length(), &buffer[0], length);

	return TO(buffer.begin(), buffer.end());*//*

	typedef std::codecvt_utf8<wchar_t> convert_type;
	std::wstring_convert<convert_type, wchar_t> converter;

	return converter.from_bytes(src);
}


template<typename TO>
TO string_cast(std::wstring src)
{
	int codePage = 1200;
	int length = ::WideCharToMultiByte(codePage, 0, src.data(), src.length(), NULL, 0, NULL, NULL);
	if (length == 0)
	{
		return TO();
	}

	std::vector<char> buffer(length);
	::WideCharToMultiByte(codePage, 0, src.data(), src.length(), &buffer[0], length, NULL, NULL);

	return TO(buffer.begin(), buffer.end());


	typedef std::codecvt_utf8<wchar_t> convert_type;
	std::wstring_convert<convert_type, wchar_t> converter;
	return converter.to_bytes(src);
}

template<typename T=int>
std::wstring string2wstring(std::string src, int codePage = CP_ACP)
{
	int length = ::MultiByteToWideChar(codePage, 0, src.data(), src.length(), NULL, 0);
	if (length == 0)
	{
		return std::wstring();
	}

	std::vector<wchar_t> buffer(length);
	::MultiByteToWideChar(codePage, 0, src.data(), src.length(), &buffer[0], length);

	return std::wstring(buffer.begin(), buffer.end());
}


template<typename T=int>
std::string wstring2string(std::wstring src, int codePage = 1200)
{
	int length = ::WideCharToMultiByte(codePage, 0, src.data(), src.length(), NULL, 0, NULL, NULL);
	if (length == 0)
	{
		return std::string();
	}

	std::vector<char> buffer(length);
	::WideCharToMultiByte(codePage, 0, src.data(), src.length(), &buffer[0], length, NULL, NULL);

	return std::string(buffer.begin(), buffer.end());
}*/