#pragma once
#include "interfaces.hpp"
#include "obj_task.hpp"
#include "m_scriptable.hpp"


class m_physical :
	public module
{
public:
	m_physical(obj*);
	virtual ~m_physical();

	virtual void update(int dt);
	virtual void init(const module&);
	virtual void new_ptr(const std::string& name);
	virtual void delete_ptr();
	virtual std::string toString() const;


	// own functions

	// when collision occurs function call m_scriptable but if he don't exist then can move obj to wright position
	bool checkCollision(obj*);
	void position(const pointF&);
	void velocity(const pointF&);
	void move(const sizeF&);

	pointF position() const;

	// IO operators
	virtual ostream& print(ostream&) const override;
	virtual istream& scan(istream&, const std::string& filename = "") override;

private:
	// obj info
	bool _collidable = true;
	bool _inertial = false;

	// connected with
	bool _updateDrawable = true;
	bool _updateScritable = false; // when collision occurs

	obj* _obj;
	rectangleF _rec;
	pointF _vel, _acc;
	pointF _vel_max = pointF(50., 50.);
	pointF _acc_max = pointF(50.,50.);
	float _bounce = 0.2f;

	circleF _bounding_circle;
};

