#include "stdafx.h"
#include "debugger.hpp"


debugger::debugger()
{
}


debugger::~debugger()
{
}

wexception::wexception()																		: _msg(L"ERROR") {};
wexception::wexception(const wstring msg, const string data, const string f, const int l)		: _msg(msg), _data(data), _file(f), _line(l){ _data = data; };
wexception::wexception(const wexception& we)													: _msg(we._msg), _data(we._data), _file(we._file), _line(we._line){};



wexception& wexception::operator=(const wexception& we)
{
	_msg = we._msg;
	_file = we._file;
	_line = we._line;
	return *this;
}


const wstring wexception::wwhat() const
{
	//wostringstream woss(_line);
	wstring r = (L"exception: ") + _msg;
	if (!_data.empty())
	{
		r += L" \r\ndata: " + string_cast<wstring>(_data);
	}
	r += L" \r\n\r\nfile: " + string_cast<wstring>(_file)+L" line: " + to_wstring(_line) + L"\r\n";
	return r;
}


const char* wexception::what() const
{
	//char buf[255];
	return (string("exception: \r\nfile: ") + _file + " line: " + to_string(_line) + ")\r\n").c_str();
}