#include "stdafx.h"
#include "m_drawable.hpp"

m_drawable::m_drawable(obj* o) : module(module_type::MODULE_DRAWABLE)
{

}


m_drawable::~m_drawable()
{

}



void m_drawable::update(int dt)
{
	// gdy obiekt jest animowany
	if (_duration > 0)
	{
		_time += dt;

		// gdy trzeba zmienic klatke, to ja zmien
		if (_time * _frameNbr.width >= _duration * (_currFrame+1))
			_currFrame = (_currFrame + 1) % _frameNbr.width;

		if (_time > _duration)
			_time %= _duration;
	}
}



void m_drawable::init(const module& m)
{
	const m_drawable* d;

	if (m.type() != type())
		throw wexception_t(G::text("badModule"), to_string(type()) + "<->" + to_string(m.type()));
	else
		d = static_cast<const m_drawable*>(&m);

	_sprite = d->_sprite;
	_position = d->_position;
	_startPoint = d->_startPoint;
	_size = d->_size;
	_offset = d->_offset;
	_frameNbr = d->_frameNbr;
	_duration = d->_duration;
}




void m_drawable::new_ptr(const string& name)
{
	wstring path = G::sets()->renderablePath + string_cast<wstring>(name)+G::sets()->renderableExtension;
	_sprite = new renderable(path.c_str());

	if (_sprite->GetWidth() < 5)
		throw wexception_t(G::text("errorBadFile"), name);
}



void m_drawable::delete_ptr()
{
	if (_sprite)
		delete _sprite;
}



std::string m_drawable::toString() const
{
	//todo:
	return "";
}



ostream& m_drawable::print(ostream& o) const
{
	loader::printStart(o, get_mod(module_type::MODULE_DRAWABLE));
	loader::print(o, type(), "type");
	loader::print(o, _startPoint, "_startPoint");
	loader::print(o, _size, "_size");
	loader::print(o, _offset, "_offset");
	loader::print(o, _frameNbr, "_frmeNbr");
	loader::print(o, _duration, "_duration");
	loader::printEnd(o);
	return o;
}



istream& m_drawable::scan(istream& i, const std::string& filename)
{
	module_type mt;

	loader::scanStart(i, filename);
	loader::scan(i, mt, filename);

	// male sprawdzanie poprawnosci typow
	if (mt != type())
		throw wexception_t(G::text("badModuleType"), filename + to_string(i.tellg()) + " module_type:" + to_string(static_cast<int>(mt)) + "->" + to_string(static_cast<int>(type())));

	loader::scan(i, _startPoint, filename);
	loader::scan(i, _size, filename);
	loader::scan(i, _offset, filename);
	loader::scan(i, _frameNbr, filename);
	loader::scan(i, _duration, filename);
	loader::scanEnd(i, filename);

	return i;
}





// class own functional
void m_drawable::action(int a)
{
	_currFrame = 0;
	_time = 0;
	_action = a % _frameNbr.height;
}


void m_drawable::draw(render_wnd* wnd)
{
	pointF srcPos;
	pointF wndPos = position(); // pos on destination window
	int res;

	// ustaw pozycje w spricie
	srcPos = _startPoint;
	srcPos.x += (_currFrame % _frameNbr.width) * _size.width;
	srcPos.y += (_action) * _size.height;

	//sprawdzanie bledow
	if (wndPos.x < -100 || wndPos.y < -100)
		return;
	else if (srcPos.x > _sprite->GetWidth() || srcPos.y > _sprite->GetHeight())
		return;
	else if (srcPos.x < -100 || srcPos.y < -100)
		return;

	// rysuj
	res = wnd->DrawImage(_sprite, static_cast<int>(wndPos.x), static_cast<int>(wndPos.y),
		static_cast<int>(srcPos.x), static_cast<int>(srcPos.y),
		static_cast<int>(_size.width), static_cast<int>(_size.height),
		Unit::UnitPixel);

	if (res != Ok)
		throw wexception_t(G::text("drawError"), string("Status: ") + to_string(res));
}




// class own get
pointF m_drawable::position() const
{
	pointF temp = _position;
	temp.x += _offset.width;
	temp.y += _offset.height;
	return temp;
}
sizeF m_drawable::size() const
{
	return _size;
}
sizeF m_drawable::offset() const
{
	return _offset;
}
int m_drawable::duration() const
{
	return _duration;
}






// class own set
void m_drawable::position(pointF p)
{
	_position.x = p.x - _offset.width;
	_position.y = p.y - _offset.height;
}


void m_drawable::size(sizeF s)
{
	_size = s;
}


void m_drawable::offset(sizeF o)
{
	_offset = o;
}


void m_drawable::sprite(renderable* i)
{
	_sprite = i;
}


void m_drawable::duration(int d)
{
	if (d < 1)
	{
		throw wexception_t(G::text("errDivByZero"), "");
	}
	else
	{
		_duration = d;
	}
}





// TASK DRAW
task_o_draw::task_o_draw(obj* o, render_wnd* r) : task(o), _ptr(r)
{}

void task_o_draw::exec()
{
	module* m = _dst->getModule(module_type::MODULE_DRAWABLE);

	if (m)
		static_cast<m_drawable*>(m)->draw(_ptr);
}