#pragma once
#include "stdafx.h"
#include "globals.hpp"
#include "simulator.hpp"


bool G::_inited = false;
std::unique_ptr<settings> G::_sets = NULL;
std::unordered_map<std::string, std::wstring> G::_text;

void G::init()
{
	if (_inited)
		return;

	textInit(_text);
	_sets = unique_ptr<settings>(new settings()); // stworzenie nowego obiektu settings

	_inited = true;
}


const std::wstring& G::text(std::string key)
{
	try
	{
		return _text.at(key);
	}
	catch (const std::out_of_range&)
	{
		std::wstring k(key.begin(), key.end());
		std::wstring msg = L"Out of Range error with key: ";
		msg.append(k);

		return msg;
	}

}

const settings* G::sets()
{
	return _sets.get();
}