#pragma once
#include "interfaces.hpp"
#include "globals.hpp"
#include "loader.hpp"


class simulator;

class m_scriptable :
	public module
{
public:
	m_scriptable(obj* o, const module_type& mt);
	virtual ~m_scriptable();


	// inrerfejs
	virtual void update(int dt) =0;
	virtual void init(const module&) override;
	virtual void new_ptr(const std::string& name) override;
	virtual void delete_ptr() override;
	virtual std::string toString() const override;


	// IO operators
	virtual ostream& print(ostream&) const override;
	virtual istream& scan(istream&, const std::string& filename = "") override;


	virtual void collision(obj* o, sizeF);
	virtual void position(pointF);
	void setSimulator(simulator* s);

protected:
	obj* _obj;
	simulator* _sim;
};

