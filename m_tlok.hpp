#pragma once
#include "m_scriptable.hpp"


class simulator;

class m_tlok :
	public m_scriptable
{
public:
	m_tlok(obj* o);// , simulator* s);
	virtual ~m_tlok();


	// inrerfejs
	virtual void update(int dt) override;
	//virtual void init(const module&) override;
	//virtual void new_ptr(const std::string& name) override;
	//virtual void delete_ptr() override;
	//virtual std::string toString() const override;


	// IO operators
	//virtual ostream& print(ostream&) const override;
	//virtual istream& scan(istream&, const std::string filename = "") override;


	virtual void collision(obj* o, sizeF);

	void reset();

private:
	time_t _time;
	bool _objDestroyed = false;
	obj* _processingObj = NULL;
	//simulator* _sim; // z m_scriptable
	//obj* _obj // z m_scriptable
};

