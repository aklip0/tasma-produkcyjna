﻿// proj1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "proj1.h"
#include "simulator.hpp"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
ULONG_PTR _gdiplusToken;
const int	TIMER_ID = 1;
float time_factor = 1.;
simulator *sim;


// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
HWND				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void				onPaint(HWND);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HWND hwnd;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PROJ1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	hwnd = InitInstance(hInstance, nCmdShow);
	if (!hwnd)
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJ1));

	/////////////////
	/// ON CREATE
	/////////////////
	SetTimer(hwnd, TIMER_ID, 10, NULL);
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&_gdiplusToken, &gdiplusStartupInput, NULL);
	sim = new simulator(hwnd);// (GetDC(hwnd));
	sim->start();

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	KillTimer(hwnd, TIMER_ID);
	GdiplusShutdown(_gdiplusToken);

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDC_PROJ1));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_PROJ1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd, btn1, txt1;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return NULL;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	CreateWindowEx(0, L"STATIC", L"Prędkość taśmy", WS_VISIBLE | WS_CHILD | SS_EDITCONTROL,
		800, 80, 100, 40, hWnd, NULL, GetModuleHandle(NULL), NULL);
	CreateWindowEx(0, L"BUTTON", L"+", WS_CHILD | WS_VISIBLE,
		820, 120, 30, 30, hWnd, (HMENU)(BTN_FASTER), NULL, NULL);
	CreateWindowEx(0, L"BUTTON", L"-", WS_CHILD | WS_VISIBLE,
		820, 150, 30, 30, hWnd, (HMENU)(BTN_SLOWER), NULL, NULL);

	return hWnd;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	int wmId, wmEvent;

	switch (message)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case BTN_FASTER:
			Beep(450, 80);
			time_factor *= 1.1f;
			break;
		case BTN_SLOWER:
			Beep(300, 80);
			time_factor /= 1.1f;
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		// TODO: Add any drawing code here...
		BeginPaint(hWnd, &ps);
		onPaint(hWnd);
		EndPaint(hWnd, &ps);
		break;
	case WM_ERASEBKGND: // nie czysc automatycznie ekranu
		break;
	case WM_TIMER:
		static time_t tim;
		sim->update(static_cast<int>(1000 * (clock() - tim) * time_factor / CLOCKS_PER_SEC));
		tim = clock();
		InvalidateRect(hWnd, NULL, TRUE);
		//RedrawWindow(hWnd, NULL, NULL, RDW_ERASENOW);
		UpdateWindow(hWnd);
		break;
	case WM_CREATE:
		PostMessage(hWnd, WM_PAINT, 0, 0);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


// realizacje double-buffering
void onPaint(HWND hwnd)
{
	HDC hdc_back;
	HBITMAP hbmp;
	HBRUSH hbrush = CreateSolidBrush(RGB(255, 255, 200));
	render_wnd* r_wnd;
	RECT rect;
	int width, height;
	int oldDC;

	if (sim == NULL)
		return;

	// pobierz szerokosc i wysokosc okna i utwirz bitmape o tych wymiarach
	GetClientRect(hwnd, &rect);
	width = rect.right - rect.left;
	height = rect.bottom - rect.top;
	hdc_back = CreateCompatibleDC(GetDC(hwnd));


	oldDC = SaveDC(hdc_back);
	hbmp = CreateCompatibleBitmap(GetDC(hwnd), width, height);
	SelectObject(hdc_back, hbmp);
	FillRect(hdc_back, &rect, hbrush);


	// renderowanie sceny
	try
	{
		r_wnd = new render_wnd(hdc_back);
		sim->set_render_wnd(r_wnd);
		task_s_draw(sim, r_wnd).exec();
		//sim->draw(r_wnd);
		delete r_wnd;
	}
	catch (const wexception& e)
	{
		MessageBox(NULL, e.wwhat().c_str(), L"ERROR", 0);
	}
	catch (const exception& e)
	{
		string m = static_cast<string>(e.what());
		MessageBoxA(NULL, m.c_str(), "ERROR", 0);
	}

	RECT clipbox;
	GetClipBox(GetDC(hwnd), &clipbox);
	BitBlt(GetDC(hwnd), clipbox.left, clipbox.top, clipbox.right - clipbox.left, clipbox.bottom - clipbox.top, hdc_back, clipbox.left, clipbox.top, SRCCOPY);
	//BitBlt(GetDC(hwnd), 0, 0, width, height, hdc_back, 0, 0, SRCAND);
	RestoreDC(hdc_back, oldDC);

	// czyszczenie
	DeleteObject(hbrush);
	DeleteObject(hbmp);
	DeleteObject(hdc_back);
}