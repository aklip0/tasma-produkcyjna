#pragma once
#include <exception>
#include "string_cast.hpp"

using namespace std;


//#define wexception_t(wmsg) wexception(wmsg,"",__FILE__,__LINE__)

// use to throw exception with wstring or strint. Macro add also __FILE__ and __LINE__
#define wexception_t(wmsg,data) wexception(wmsg,data,__FILE__,__LINE__)
#define LOG(msg)	

class wexception : public exception
{
public:
	wexception();
	wexception(const wstring, const string, const string, const int); // msg, data, file, line
	wexception(const wexception&);

	wexception& operator=(const wexception&);
	const wstring wwhat() const;
	const char* what() const override;

protected:
	wstring _msg;
	string _file;
	string _data;
	int _line;
};



class logger
{
};


class debugger
{
public:
	debugger();
	virtual ~debugger();
};

