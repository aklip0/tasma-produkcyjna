#pragma once
#include "stdafx.h"
#include "globals.hpp"


class obj;
class module;
class game_map;

class loader
{
public:
	loader();
	~loader();

	void load(istream& file, obj&, const string filename = "");
	void load(string path, game_map&);

	//template<typename T, template<class, class...> class C, class... Args>
	//static istream& scan(istream& i, C<T, Args...>& data, const std::string filename = "");
	template<typename T>
	static istream& scan(istream& i, T& data, const std::string filename = "");
	static istream& scanStart(istream& i, const std::string filename = "");
	static istream& scanEnd(istream& i, const std::string filename = "");

	template<typename T>
	static ostream& print(ostream& o, const T& data, const std::string name = ""); // name to opcjonalna nazwa zmiennej do zapisania
	static ostream& printStart(ostream& o, std::string name = ""); // name to opcjonalna nazwa modulu do zapisania
	static ostream& printEnd(ostream& o);

private:
	bool hasOption(const string in, const string opt) const;
	bool loadObjSwitch(istream&);
};




//////////////////////
/// IMPLEMENTACJA ////
//////////////////////



//template<typename T, template<class, class...> class C, class... Args>
//istream& loader::scan(istream& i, C<T, Args...>& data, const std::string filename)  // http://stackoverflow.com/questions/213761/what-are-some-uses-of-template-template-parameters-in-c
template<typename T>
istream& loader::scan(istream& i, T& data, const std::string filename)
{
	streamoff pos = i.tellg();
	string msg;

	try
	{
		i >> data;
	}
	catch (exception& e)
	{
		msg = "message:";
		msg+= e.what();
		msg+= "\nfile: ";
	}

	if (i.fail())
		throw wexception_t(G::text("errorBadFile"), msg + filename + to_string(pos));

	i.ignore(0xffffffff, '\n');

	return i;
};


template<typename T>
ostream& loader::print(ostream& o, const T& data, const std::string name)
{
	return o << data << "\t\t#" << name << "\n";
}
