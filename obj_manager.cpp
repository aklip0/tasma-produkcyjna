#include "stdafx.h"
#include "obj_manager.hpp"
#include <fstream>


obj_manager::obj_manager(simulator* s) : manager(manager_type::OBJ_MANAGER), _sim(s)
{
	obj* o = new obj(obj_type::DYNAMIC_OBJ);
	addModule(MODULE_DRAWABLE, o);
	addModule(MODULE_PHYSICAL, o);
	addModule(MODULE_FACTORY, o);
	std::ofstream f("data/t.txt", ios::trunc);
	o->print(f);
	f.close();
}


obj_manager::~obj_manager()
{
	obj_table::iterator it;
	map<string, obj*>::iterator dit;

	// usun najpierw obiekty
	for (it = _objects.begin(); it != _objects.end(); ++it)
		delete *it;

	// a potem ich definicje ze wszystkimi danymi
	for (dit = _obj_defs.begin(); dit != _obj_defs.end(); ++dit)
	{
		(*it)->delete_ptr();
		delete (*it);
	}
}







/// najwazniejsza funkcja
/// w pelni dodaje obiekty, a na koniec je usuwa
obj* obj_manager::createObj(const std::string& name, const pointF& position)
{
	obj* o;
	obj_type ot;

	ot = get_ot(name); // pobranie object_class_type z pamieci(mapy) lub pliku
	o = new obj(ot);

	switch (ot)
	{
	case obj_type::STATIC_OBJ:
			addModule(MODULE_DRAWABLE, o);
			o->init(get_obj_def(name, o));
			break;
		case obj_type::DYNAMIC_OBJ:
			addModule(MODULE_PHYSICAL, o);
			addModule(MODULE_DRAWABLE, o);
			o->init(get_obj_def(name, o));
			break;
		case obj_type::TRIANGLE_OBJ:
			addModule(MODULE_PHYSICAL, o);
			addModule(MODULE_DRAWABLE, o);
			o->init(get_obj_def(name, o));
			break;
		case obj_type::RECTANGLE_OBJ:
			addModule(MODULE_PHYSICAL, o);
			addModule(MODULE_DRAWABLE, o);
			o->init(get_obj_def(name, o));
			break;
		case obj_type::TLOK_OBJ:
			addModule(MODULE_PHYSICAL, o);
			addModule(MODULE_TLOK, o);
			addModule(MODULE_DRAWABLE, o);
			static_cast<m_scriptable*>(o->getModule(MODULE_SCRIPTABLE))->setSimulator(_sim);
			o->init(get_obj_def(name, o));
			break;
		case obj_type::PIEC_OBJ:
			addModule(MODULE_FACTORY, o);
			static_cast<m_scriptable*>(o->getModule(MODULE_SCRIPTABLE))->setSimulator(_sim);
			o->init(get_obj_def(name, o));
			break;
		case obj_type::KONIEC_OBJ:
			addModule(MODULE_PHYSICAL, o);
			addModule(MODULE_TERMINATOR, o);
			static_cast<m_scriptable*>(o->getModule(MODULE_SCRIPTABLE))->setSimulator(_sim);
			o->init(get_obj_def(name, o));
			break;
		default:
			throw wexception_t(G::text("unknownObject"), name);
			break;
	}


	_objects.push_back(o);
	task_o_set_position(o, position).exec();
	++_obj_counter;
	return o;
}



void obj_manager::update(int dt)
{
	obj_table::iterator it;

	for (it = _objects.begin(); it != _objects.end(); ++it)
		(*it)->update(dt);

	updateTask();

	checkCollision();
}



void obj_manager::checkCollision()
{
	obj_table::iterator it, sit;
	m_physical* mp;

	// przelec po wszystkich obiektach
	for (it = _objects.begin(); it != _objects.end(); ++it)
	{
		// gdy obiekt nie ma modulu physical // errata: wtedy gdy w tab najpierw bedzie static, potem dynamic i bedzie to kolizja, to jej nie wykryjemy
		//if ((*it)->getModule(module_type::MODULE_PHYSICAL) == NULL)
		//	continue;

		for (sit = next(it); sit != _objects.end(); ++sit)
		{
			mp = static_cast<m_physical*>((*sit)->getModule(module_type::MODULE_PHYSICAL));

			if (mp != NULL)
				mp->checkCollision(*it);
		}
	}
}



void obj_manager::loadMap(const string& name)
{
	ifstream file(G::sets()->dataPath + name + G::sets()->dataExtension, ios::in | ios::beg);
	stringstream ss;
	string temp = "";
	pointF pos;
	obj* new_o;

	if (file.fail())
		throw wexception_t(G::text("noFileFound"), name + G::sets()->dataExtension);

	// przewin naglowek
	while (temp.find(G::sets()->fileEnd) == string::npos);
		getline(file, temp);


	loader::scanStart(file);
	getline(file, ss.str());

	while (ss.str().find(G::sets()->fileEnd) == string::npos)
	{
		ss >> temp >> pos;
		new_o = createObj(name);
		//new_o->
		getline(file, ss.str());
	}
}



obj* obj_manager::get_obj_def(const std::string& name, obj* pattern)
{
	map<string, obj*>::iterator el = _obj_defs.find(name);

	//  gdy juz byl urzywany
	if (el != _obj_defs.end())
		return el->second;
	else
	{ // wczytaj dane z pliku
		vector<module*>::iterator mod;
		obj_type ot = get_ot(name);
		obj* new_obj = new obj(ot);

		for (mod = pattern->_modules.begin(); mod != pattern->_modules.end(); ++mod)
			addModule((*mod)->type(), new_obj);

		_obj_defs.insert(pair<string, obj*>(name, new_obj));
		new_obj->init(name);
		return new_obj;
	}
}




obj_type obj_manager::get_ot(std::string name)
{
	obj_type ot = _obj_type[name];

	// gdy taka wartosc juz byla, to ja zwroc
	if (ot != obj_type())
		return ot;
	else
	{
		name = G::sets()->dataPath + name + G::sets()->dataExtension;
		ifstream file(name);

		if (file.fail())
			throw wexception_t(G::text("noFileFound"), name);

		loader::scanStart(file);
		loader::scan(file, ot, name);
		file.close();
		return ot;
	}
}

obj_table::iterator obj_manager::firstObj()
{
	return _objects.begin();
}

bool obj_manager::nextObj(obj_table::iterator& it)
{
	if (it == _objects.end())
		return false;

	++it;

	if (it == _objects.end())
		return false;
	else
		return true;
}


void obj_manager::addModule(const module_type& mt, obj* o)
{
	o->_modules.push_back(module::create(mt, o));
}

void obj_manager::delObj(obj* o)
{
	obj_table::iterator it = find(_objects.begin(), _objects.end(), o);

	if (it != _objects.end())
	{
		_objects.erase(it);
		--_obj_counter;
	}
}

void obj_manager::addTask(task<obj_manager>* pt)
{
	if (pt == NULL)
		return;

	_tasks.push_back(pt);
}

class taskFiltrOM
{
	time_t _thres;
public:
	taskFiltrOM(time_t th) : _thres(th) {}
	bool operator()(task<obj_manager>* t)
	{
		if (t->getDeadtime() <= _thres)
			return true; // remove
		else
			return false;
	}
};

void obj_manager::updateTask()
{
	vector<task<obj_manager>*>::reverse_iterator rit;
	vector<task<obj_manager>*>::iterator new_end;
	time_t cl = clock();
	taskFiltrOM filtr(cl);


	for (rit = _tasks.rbegin(); rit != _tasks.rend(); ++rit)
	{
		if ((*rit)->getDeadtime() <= cl)
		{
			(*rit)->exec();
			//--rit;
			delete *rit; // usun obiekt task
		}

	}

	new_end = remove_if(_tasks.begin(), _tasks.end(), filtr);
	_tasks.erase(new_end, _tasks.end()); // to resize
}



//////////////////////////////////
/////////// TASKS ////////////////
//////////////////////////////////

void task_om_draw::exec()
{
	obj_table::iterator it = _dst->firstObj();

	do
	{
		task_o_draw(*it, _ptr).exec(); // todo usun exec()
	}
	while (_dst->nextObj(it));
}



void task_om_checkCollision_all::exec()
{
	obj_table::iterator it = _dst->firstObj();

	do
	{
		if ((*it)->getModule(module_type::MODULE_PHYSICAL) != NULL)
			task_om_checkCollision_obj(_dst, it).exec(); // todo usun exec()
	} while (_dst->nextObj(it));
}



void task_om_checkCollision_obj::exec()
{
	obj_table::iterator it = next(it);
	module* mp;

	do
	{
		mp = (*it)->getModule(module_type::MODULE_PHYSICAL);
		if (mp != NULL)
			static_cast<m_physical*>((*_it)->getModule(MODULE_PHYSICAL))->checkCollision(*it);


	} while (_dst->nextObj(_it));
}



void task_om_load_map::exec()
{
	string obj_name;
	pointF obj_pos;
	ifstream file(G::sets()->dataPath + _filename + G::sets()->dataExtension);

	if (file.fail())
		throw wexception_t(G::text("noFileFound"), G::sets()->dataPath + _filename + G::sets()->dataExtension);

	loader::scanStart(file);

	// najpierw zaladuj nazwe, aby sprawdzic od czego sie zaczyna nowa linijka, czy jest to fileEnd?
	file >> obj_name;

	// glowna petla ladowania obiektow
	while (obj_name != G::sets()->fileEnd)
	{
		file >> obj_pos;
		file.ignore(0xffffff, '\n');
		_dst->createObj(obj_name, obj_pos);

		// gdy juz wszystko zrobisz, to zaladuj nastepna linijke
		file >> obj_name;
	}
}


void task_om_del_obj::exec()
{
	_dst->delObj(_obj);
}