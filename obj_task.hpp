#pragma once
#include "stdafx.h"
#include "globals.hpp"
#include "interfaces.hpp"
#include "loader.hpp"



////////////////////////
///////// TASK /////////
////////////////////////

class task_o_set_position : public task<obj>
{
	const pointF _point;
public:
	task_o_set_position(obj* o, const pointF p) : task(o), _point(p) {}
	virtual void exec() override;
};




// DRAWABLE
class task_o_draw : public task < obj >
{
	render_wnd* _ptr;
public:
	task_o_draw(obj*, render_wnd*);
	virtual void exec() override;
};



// PHYSICAL
class task_o_set_velocity : public task < obj >
{
	pointF _vel;
public:
	task_o_set_velocity(obj* o, const pointF& v) : task(o), _vel(v) {}
	task_o_set_velocity(obj* o, const float x, const float y) : task(o), _vel(x, y) {}
	virtual void exec() override;
};


class task_o_checkCollision : public task < obj >
{
	obj* _obj;
	task_o_checkCollision();
public:
	task_o_checkCollision(obj* s, obj* d) : task(s), _obj(d) {}
	virtual void exec() override;
};