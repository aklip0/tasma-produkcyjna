#include "stdafx.h"
#include "m_tlok.hpp"
#include "m_drawable.hpp"
#include "m_physical.hpp"
#include "simulator.hpp"


m_tlok::m_tlok(obj* o) : m_scriptable(o, MODULE_TLOK)
{
}


m_tlok::~m_tlok()
{
}


// inrerfejs
void m_tlok::update(int dt)
{
	m_drawable* tmd; // tlok module drawable
	m_drawable* pomd = NULL; // processing object module drawable

	// gdy masz odpoczywac, to nic nie rob
	if (_time < 0)
		return;

	if (_time > 0)
		_time += dt;

	tmd = static_cast<m_drawable*>(_obj->getModule(module_type::MODULE_DRAWABLE));

	if (_processingObj)
		pomd = static_cast<m_drawable*>(_processingObj->getModule(module_type::MODULE_DRAWABLE));
	

	// gdy obiekt dojechal do polowy tloka, to go zatrzymaj
	if (_time == 0 && _processingObj && pomd->position().x >= tmd->position().x)
	{
		_time = 1;
		static_cast<m_physical*>(_processingObj->getModule(MODULE_PHYSICAL))->velocity(pointF(0, 0));
		task_o_set_position(_processingObj, static_cast<m_physical*>(_obj->getModule(MODULE_PHYSICAL))->position()).exec();
		static_cast<m_drawable*>(_obj->getModule(module_type::MODULE_DRAWABLE))->action(1);
		//static_cast<m_drawable*>(_obj->getModule(module_type::MODULE_DRAWABLE))->update(0);
	}

	// po zamknieciu tloka zniszcz obiekt
	if (_objDestroyed == false && tmd->duration() < 2 * _time)
	{
		//todo usun _processingObj
		obj_manager* om = static_cast<obj_manager*>(_sim->getManager(manager_type::OBJ_MANAGER));
		om->addTask(new task_om_del_obj(om, _processingObj));
		_objDestroyed = true;
		_processingObj = NULL;
	}
	// gdy tlok zrobi cykl
	else if (tmd->duration() < _time)
	{
		tmd->action(0);
		reset();
	}

}


void m_tlok::collision(obj* o, sizeF inter)
{
	//odrzucanie kwadrtow
	if (_processingObj==NULL && o->getType() == obj_type::TRIANGLE_OBJ)
	{
		_processingObj = o;
		_time = 0;
		_objDestroyed = false;
	}
}


void m_tlok::reset()
{
	_processingObj = NULL;
	_time = -1;
	_objDestroyed = false;
}