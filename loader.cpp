#include "stdafx.h"
#include "loader.hpp"


loader::loader()
{
}


loader::~loader()
{
}

void loader::load(istream& file, obj& o, string filename)
{
	stringstream ss;
	string str; // object class type

	if (file.fail())
	{
		throw wexception_t(G::text("noFileFound"), filename+to_string(file.tellg()));
	}

	// ladowanie obrazka

}


istream& loader::scanStart(istream& i, const string filename)
{
	streamoff pos = i.tellg(); // na wypadek bledu
	std::string st;
	i >> st;

	// pomin komentarze
	while (st.empty() || st[0] == '#')
	{
		i.ignore(0xffffff, '\n');
		i >> st;

		if (!i.good())
			throw_parsing_error(string_cast<std::string>(G::text("badParsing")) + "\nfilename: " + filename, pos, __LINE__);
	}

	// porownaj zawartosc pliku z ustawieniami
	if (st != G::sets()->fileStart)
		throw_parsing_error(string_cast<std::string>(G::text("badParsing")) + "\nfilename: " + filename, pos, __LINE__);

	i.ignore(0xffffff, '\n');
	return i;
}

istream& loader::scanEnd(istream& i, const string filename)
{
	streamoff pos = i.tellg();
	std::string st;
	i >> st;

	if (st != G::sets()->fileEnd)
		throw_parsing_error(string_cast<std::string>(G::text("badParsing")) + "\nfilename: " + filename, pos, __LINE__);

	i.ignore(0xffffffff, '\n');

	return i;
}

ostream& loader::printStart(ostream& o, std::string name)
{
	return o << G::sets()->fileStart << " #" << name << "\n";
}

ostream& loader::printEnd(ostream& o)
{
	return o << G::sets()->fileEnd << "\n";
}