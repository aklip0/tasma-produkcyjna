#pragma once
#include "stdafx.h"

template<typename T=std::wstring>
void textInit(std::unordered_map<std::string, T> &tx)
{
	tx.insert({ "hello", L"No cze��" });
	tx.insert({ "error", L"B��d" });
	tx.insert({ "errorInObjFabric", L"B��d przy tworzeniu nowego obiektu, b�edna nazwa " });
	tx.insert({ "errDivByZero", L"Nie wolno dzieli� przez zero! " });
	tx.insert({ "unknownObject", L"Takiego obiektu nie znam..." });
	tx.insert({ "unknownModule", L"Takiego modu�u nie znam..." });
	tx.insert({ "badModule", L"co� tu si� pomiesza�o z tymi modu�ami..." });
	tx.insert({ "badModuleScriptable", L"B��w w module SCRIPTABLE, prawdopodobnie _sim==NULL" });
	tx.insert({ "unamatchedOCT", L"Nie pasuj�cy obj_class_type" });
	tx.insert({
	{ "render_wndError", L"Brak ustawionego render_wnd" },
	{ "drawError", L"B��d przy rusowaniu obrazka" },
	{ "unknownMessage", L"B��dna wiadomo��" }
	});

	// PLIKI
	tx.insert({ "errorBadFile", L"Problemy z plikiem " });
	tx.insert({ "noFileFound", L"Nie odnaleziono pliku... " });
	tx.insert({ "badParsing", L"B��d przy parsowaniu pliku " });
	tx.insert({ "badModuleType", L"B�edny typ modu�u podanego do parsowania " });
}