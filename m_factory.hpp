#pragma once
#include "m_scriptable.hpp"



class m_factory :
	public m_scriptable
{
public:
	m_factory(obj*);
	virtual ~m_factory();

	virtual void init(const module&) override;
	virtual void update(int dt) override;
	virtual ostream& print(ostream&) const override;
	virtual istream& scan(istream&, const string& filename) override;


	virtual void position(pointF) override;

private:
	pointF _pos;
	int _duration;
	int _time = 0;
};

