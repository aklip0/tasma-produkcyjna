#include "stdafx.h"
#include "interfaces.hpp"
#include "m_drawable.hpp"
#include "m_physical.hpp"
#include "m_tlok.hpp"
#include "m_factory.hpp"
#include "m_terminator.hpp"



map<string, module_type> module::_get_mod_type;

//todo: fill and use it
void module::init()
{
	_get_mod_type.insert({
	{ "MODULE_NONE", MODULE_NONE },
	{ "MODULE_ALL", MODULE_ALL },
	{ "MODULE_DRAWABLE", MODULE_DRAWABLE },
	{ "MODULE_PHYSICAL", MODULE_PHYSICAL },
	{ "MODULE_SCRIPTABLE", MODULE_SCRIPTABLE },
	{ "MODULE_TLOK", MODULE_TLOK },
	{ "MODULE_FACTORY", MODULE_FACTORY },
	{ "MODULE_TERMINATOR", MODULE_TERMINATOR }
	});
}

// module factory
module* module::create(const module_type& mt, obj* o)
{
	switch (mt)
	{
	case module_type::MODULE_DRAWABLE:
		return new m_drawable();
		break;
	case module_type::MODULE_PHYSICAL:
		return new m_physical(o);
		break;
	case module_type::MODULE_TLOK:
		return new m_tlok(o);
		break;
	case module_type::MODULE_FACTORY:
		return new m_factory(o);
		break;
	case module_type::MODULE_TERMINATOR:
		return new m_terminator(o);
		break;
	default:
		throw wexception_t(G::text("unknownModule"), get_mod(mt));
		break;
	}
	return NULL;
}





module::module(module_type tp, bool scriptable) : _type(tp), _scriptable(scriptable)
{
}


module::~module()
{
}






module_type module::type() const
{
	return _type;
}

bool module::isScriptable() const
{
	return _scriptable;
}



module* module::clone() const
{
	module* nm = create(type());
	nm->init(*this);
	return nm;
}


void module::init(const module& m)
{
	if (m.type() != type())
		throw wexception_t(G::text("badModule"), to_string(type()) + "<->" + to_string(m.type()));

	*this = m;
}



module_type module::get_mod(const string& str)
{
	map<string, module_type>::iterator it;

	// lazy initioation
	if (_get_mod_type.size() == 0)
		module::init();

	it = _get_mod_type.find(str);

	if (it == _get_mod_type.end())
		return module_type::MODULE_NONE;
	else
		return it->second;
}

const string& module::get_mod(const module_type& mt)
{
	map<string, module_type>::iterator it;

	// lazy initiation
	if (_get_mod_type.size() == 0)
		module::init();

	for (it = _get_mod_type.begin(); it != _get_mod_type.end(); ++it)
		if (it->second == mt)
			return it->first;
	

	// jezeli nie znajdziesz, to wywolaj rekurencyje
	return get_mod(module_type::MODULE_NONE);
}






ostream& operator<<(ostream& o, const module& m)
{
	return m.print(o);
}

istream& operator>>(istream& i, module& m)
{
	return m.scan(i);
}

istream& operator>>(istream& i, module_type& mt)
{
	int in;
	i >> in;
	mt = static_cast<module_type>(in);
	return i;
}