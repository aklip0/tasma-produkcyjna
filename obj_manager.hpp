#pragma once
#include "stdafx.h"
#include "globals.hpp"
#include "interfaces.hpp"
#include "loader.hpp"
#include "obj_task.hpp"

//module list
#include "m_drawable.hpp"
#include "m_scriptable.hpp"
#include "m_physical.hpp"

typedef std::vector<obj*> obj_table;


class simulator;

class obj_manager : public manager
{
public:
	obj_manager(simulator*);
	~obj_manager();

	obj* createObj(const std::string& name, const pointF& position = pointF(0.f, 0.f));
	void addModule(const module_type&, obj*);
	void delObj(obj*); // todo

	obj_table::iterator firstObj();
	bool nextObj(obj_table::iterator&); // zwraca false dla end()

	void addTask(task<obj_manager>*);
	void loadMap(const string& name);

	virtual void update(int dt) override;
	virtual void updateTask();
	virtual void checkCollision();

private:
	// prywatne funkcje
	obj* get_obj_def(const std::string& name, obj* pattern);
	obj_type get_ot(std::string name);

	// pola klasy
	simulator* _sim;
	obj_table _objects;
	std::map<std::string, obj*> _obj_defs;
	std::map<std::string, obj_type> _obj_type;
	std::vector<task<obj_manager>*> _tasks;
	int _obj_counter = 0;
};






// WYWOLUJE task_o_draw.exec() dla kazdego obiektu
class task_om_draw : public task < obj_manager >
{
	render_wnd* _ptr;
public:
	task_om_draw(obj_manager* dst, render_wnd* wnd) : task(dst), _ptr(wnd) {}

	// WYWOLUJE task_o_draw.exec() dla kazdego obiektu
	virtual void exec() override;
};



class task_om_checkCollision_all : public task < obj_manager >
{
public:
	task_om_checkCollision_all(obj_manager* dst) : task(dst) {}

	// sprawdza metoda brutal force
	virtual void exec() override;
};




class task_om_checkCollision_obj : public task < obj_manager >
{
	obj_table::iterator _it;
public:
	task_om_checkCollision_obj(obj_manager* dst, obj_table::iterator it) : task(dst), _it(it) {}

	// sprawdza kolizje elementu z iteratora z wszystkimi nastepnymi
	virtual void exec() override;
};



class task_om_load_map : public task < obj_manager >
{
	string _filename;
public:
	task_om_load_map(obj_manager* dst, string filename) : task(dst), _filename(filename) {}

	// urzywa obj_manager->createObj(name, pos);
	virtual void exec() override;
};


class task_om_del_obj : public task < obj_manager >
{
	obj* _obj;
public:
	task_om_del_obj(obj_manager* dst, obj* toDel) : task(dst), _obj(toDel) {}

	virtual void exec() override;
};