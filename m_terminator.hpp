#pragma once
#include "m_scriptable.hpp"
class m_terminator :
	public m_scriptable
{
public:
	m_terminator(obj* o);
	virtual ~m_terminator();

	virtual void init(const module& m) override {};
	virtual void update(int dt) override;
	virtual void collision(obj*, sizeF) override;

	virtual ostream& print(ostream&) const override;
	virtual istream& scan(istream&, const string& filename) override;
};

