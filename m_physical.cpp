#include "stdafx.h"
#include "m_physical.hpp"
#include "m_drawable.hpp"


m_physical::m_physical(obj* o) : module(module_type::MODULE_PHYSICAL), _obj(o)
{
	if (o == NULL)
		throw wexception_t(G::text("unknownObject"), "m_physical o = NULL");

	if (o->getModule(module_type::MODULE_DRAWABLE) != NULL)
		_updateDrawable = true;
}


m_physical::~m_physical()
{
}



// VIRTUALS

void m_physical::update(int dt)
{
	m_drawable* md;
	float ms = dt / 1000.f;

	//if (_inertial)
	_vel += _acc * ms;

	_rec.move(_vel.x*ms, _vel.y*ms);

	if (_updateDrawable && (md = static_cast<m_drawable*>(_obj->getModule(MODULE_DRAWABLE))) != NULL)
		md->position(_rec.position());
		//task_o_set_position(_obj, _rec.position()).exec();
}



void m_physical::init(const module& m)
{
	const m_physical* p;

	if (m.type() != module_type::MODULE_PHYSICAL)
		throw wexception_t(G::text("badModule"), get_mod(module_type::MODULE_DRAWABLE) + "<->" + get_mod(m.type()));

	p = static_cast<const m_physical*>(&m);
	
	_updateDrawable = p->_updateDrawable;
	_vel_max = p->_vel_max;
	_acc_max = p->_acc_max;
	_vel = p->_vel;
	_acc = p->_acc;
	_rec = p->_rec;
	_bounce = p->_bounce;
}



void m_physical::new_ptr(const std::string& name)
{

}



void m_physical::delete_ptr()
{

}



std::string m_physical::toString() const
{
	stringstream ss("m_physical");
	print(ss);
	return ss.str();
}





// own functions
bool m_physical::checkCollision(obj* o)
{
	sizeF inter;
	m_physical* op = static_cast<m_physical*>(o->getModule(module_type::MODULE_PHYSICAL));	
	m_scriptable *os, *ms;

	if (op == NULL)
		return false;

	inter = op->_rec.intersects(_rec);

	if (inter.width == 0 && inter.height == 0)
		return false;
	else
	{ // gdy mamy kolizje, wybierz as w ktorej trzeba obiekty slabiej przesunac
		if (abs(inter.width) > abs(inter.height))
			inter.width = 0;
		else
			inter.height = 0;
	}


	ms = static_cast<m_scriptable*>(_obj->getModule(module_type::MODULE_SCRIPTABLE));
	os = static_cast<m_scriptable*>(o->getModule(module_type::MODULE_SCRIPTABLE));


	// gdy ktos posiada modul scriptable
	if (ms != NULL)
		ms->collision(o, inter);
	else if (os != NULL)
	{
		inter.scale(-1.f);
		os->collision(_obj, inter);
	}
	// gdy nikt go nie posiada
	else
	{
		// kolizja z bloczkiem
		if (o->getType() == obj_type::STATIC_OBJ)
		{
			// przesun obiekt i 'odbij' predkosc
			this->move(inter);
		}
		// kolizja z obiektem ruchomym
		else
		{
			//inter.scale(0.5f);
			this->move(inter);
			inter.scale(-1.0f);
			op->move(inter);
			if (abs(inter.width) < abs(inter.height))
				op->_vel.y *= -_bounce * op->_bounce;
			else
				op->_vel.x *= -_bounce * op->_bounce;
		}
		
		// odbij swoja predkosc
		if (abs(inter.width) < abs(inter.height))
			_vel.y *= -_bounce * op->_bounce;
		else
			_vel.x *= -_bounce * op->_bounce;
	}

	return true;
}


void m_physical::position(const pointF& p)
{
	_rec.position(p);

	//if (_updateDrawable)
	//	static_cast<m_drawable*>(_obj->getModule(module_type::MODULE_DRAWABLE))->position(p);
}


void m_physical::velocity(const pointF& v)
{
	if (abs(v.x) > _vel_max.x)
		_vel.x = _vel_max.x * sgn(v.x);
	else
		_vel.x = v.x;

	if (abs(v.y) > _vel_max.y)
		_vel.y = _vel_max.y * sgn(v.y);
	else
		_vel.y = v.y;
}


void m_physical::move(const sizeF& m)
{
	m_drawable* md = static_cast<m_drawable*>(_obj->getModule(module_type::MODULE_DRAWABLE));


	_rec.move(m.width, m.height);

	if (md != NULL)
		md->position(_rec.position());
}


pointF m_physical::position() const
{
	return _rec.position();
}




// IO operators
ostream& m_physical::print(ostream& o) const
{
	loader::printStart(o, get_mod(module_type::MODULE_PHYSICAL));
	loader::print(o, type(), "type");
	loader::print(o, _rec.Size(), "_size");
	loader::print(o, _vel, "_vel");
	loader::print(o, _vel_max, "_vel_max");
	loader::print(o, _acc, "_acc");
	loader::print(o, _acc_max, "_acc_max");
	loader::print(o, _bounce, "_bounce");
	loader::printEnd(o);
	return o;
}



istream& m_physical::scan(istream& i, const std::string& filename)
{
	module_type mt;
	sizeF si;

	loader::scanStart(i, filename);
	loader::scan(i, mt, filename);

	if (mt != type())
		throw wexception_t(G::text("badModuleType"), filename + to_string(i.tellg()) + " module_type:" + to_string(static_cast<int>(mt)) + "->" + to_string(static_cast<int>(type())));

	loader::scan(i, si, filename);
	loader::scan(i, _vel, filename);
	loader::scan(i, _vel_max, filename);
	loader::scan(i, _acc, filename);
	loader::scan(i, _acc_max, filename);
	loader::scan(i, _bounce, filename);
	loader::scanEnd(i, filename);

	_rec.Size(si);
	return i;
}



///////////////////
////// TASK ///////
///////////////////

void task_o_set_velocity::exec()
{
	m_physical* md = static_cast<m_physical*>(_dst->getModule(module_type::MODULE_PHYSICAL));

	md->velocity(_vel);
}