#pragma once
#include "stdafx.h"
#include "globals.hpp"
#include "loader.hpp"
#include "message.hpp"

enum module_type
{
	MODULE_ALL = -1,
	MODULE_NONE = 0, // wartosc domyslna
	MODULE_DRAWABLE = 1, // != 0, bo 0 jest wartoscia domyslna
	MODULE_PHYSICAL,
	MODULE_SCRIPTED
};


// interfejs
class module
{
public:
	virtual ~module();

	// inrerfejs
	virtual void update(render_wnd&, int dt) = 0;
	virtual void init(const module&);
	virtual void new_ptr(const std::string& name) = 0;
	virtual void delete_ptr() = 0;
	virtual std::string toString() const = 0;

	//template<typename... args>
	//virtual bool message(const message& msg, const module_type& target, args ...) =0;

	// IO operators
	virtual ostream& print(ostream&) const = 0;
	virtual istream& scan(istream&, const std::string filename = "") = 0;
	friend ostream& operator<<(ostream&, const module&);
	friend istream& operator>>(istream&, module&);

	// own functions
	module_type type() const;
	module* clone() const;

	// static functions
	static void init();
	static module* create(const module_type&);
	static module_type get_mod(const string&);
	static const string& get_mod(const module_type&);

protected:
	// private constructor!!
	// it can be created by module::create(string)
	module(module_type);

private:
	// truly const value, because there is only get methode and private
	// perhaps only init method may overwrite _type
	module_type _type;

	// static fields
	static map<string, module_type> _get_mod_type;
};

istream& operator>>(istream& i, module_type&);