#pragma once
#include "settings.hpp"
#include "text.hpp"
#include "debugger.hpp"

// GRAPHICS LIBRARY TYPES
typedef Graphics	render_wnd;
typedef Image		renderable;
//typedef manager	task_manager;


class G
{
public:
	static void init();

	//zwraca tekst z bazy na podstawie key lub zwraca tekst b�edu
	static const std::wstring& text(std::string key);
	static const settings* sets();

private:
	static bool _inited;
	static std::unique_ptr<settings> _sets;
	static std::unordered_map<std::string, std::wstring> _text;
};