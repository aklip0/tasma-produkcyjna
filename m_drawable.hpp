#pragma once
#include "stdafx.h"
#include "globals.hpp"
#include "interfaces.hpp"
#include "obj_task.hpp"
#include "loader.hpp"


class m_drawable : public module
{
public:
	~m_drawable();

	// from intrerface

	//update zmienia klatke animacji
	virtual void update(int dt) override;
	virtual void init(const module&) override;
	virtual void new_ptr(const std::string& name) override;
	virtual void delete_ptr() override;
	virtual std::string toString() const override;

	// IO operators
	virtual istream& scan(istream&, const std::string& filename = "") override;
	virtual ostream& print(ostream&) const override;


	// class own functional

	// set new aninmation - new row in _sprite
	void action(int);
	void draw(render_wnd*);

	// class own get
	pointF position() const;
	sizeF size() const;
	sizeF offset() const;
	int duration() const;

	// class own set
	void position(pointF);
	void size(sizeF);
	void offset(sizeF);
	void sprite(renderable*);
	void duration(int);

	friend class module;


protected:
	//private contructor!!
	// it can be created by module::create(string)
	m_drawable(obj* = NULL); // doesn't use this ptr


private:


	pointF _position;
	pointF _startPoint; // start point in _sprite (LT corner)
	sizeF _size = sizeF(40.f,40.f);
	sizeF _offset;
	sizeI _frameNbr = sizeI(1,1);
	int _duration=-1, _time=0, _currFrame=0, _action=0;
	renderable* _sprite = NULL;
};
