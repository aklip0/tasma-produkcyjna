#include "stdafx.h"
#include "interfaces.hpp"
#include "loader.hpp"
#include "obj_task.hpp"
#include "m_drawable.hpp"
#include "m_physical.hpp"
#include "m_scriptable.hpp"


istream& operator>>(istream& i, obj_type& ot)
{
	int n;
	i >> n;
	ot = static_cast<obj_type>(n);
	return i;
}


obj::obj(obj_type ot) : _ot(ot)
{
}


obj::~obj()
{
	module_container::const_iterator it;

	for (it = _modules.begin(); it != _modules.end(); ++it)
		delete (*it);
}


/*template<typename... args>
virtual bool obj::message(const message& msg, const module_type& target)
{
	switch (msg)
	{
	case message::DRAW:
		break;
	default:
		throw wexception_t(G::text("unknownMessage"), to_string(module::get_mod(target) + "->" + to_string(msg));
		break;
	}
}*/


void obj::update(int dt)
{
	vector<module*>::iterator it;

	for (it = _modules.begin(); it != _modules.end(); ++it)
		(*it)->update(dt);
}


void obj::init(const obj* pattern)
{
	vector<module*>::const_iterator p_it = pattern->_modules.begin();
	vector<module*>::iterator it = _modules.begin();

	for (; it != _modules.end(); ++it, ++ p_it)
		(*it)->init(**p_it);
}


// laduje wszytkie moduly po kolei
// to jest ladowanie definicji obiektu (bo na podstawie stringa)
void obj::init(const std::string obj_name)
{
	std::ifstream file;
	module_container::const_iterator it;

	file.open(G::sets()->dataPath + obj_name + G::sets()->dataExtension, ios::in | ios::beg);

	if (file.fail())
		throw wexception_t(G::text("noFileFound"), obj_name);

	// zczytaj standardowe dane z pliku
	scan(file, obj_name + G::sets()->dataExtension);

	// ladowanie obiektow wspolnych
	for (it = _modules.begin(); it != _modules.end(); ++it)
		(*it)->new_ptr(obj_name);
}


void obj::delete_ptr()
{
	module_container::const_iterator it;

	for (it = _modules.begin(); it != _modules.end(); ++it)
		(*it)->delete_ptr();
}


module* obj::getModule(const module_type& mt)
{
	module_container::iterator it;

	if (mt != module_type::MODULE_SCRIPTABLE)
	{
		for (it = _modules.begin(); it != _modules.end(); ++it)
			if ((*it)->type() == mt)
				return *it._Ptr;
	}
	else
	{
		for (it = _modules.begin(); it != _modules.end(); ++it)
			if ((*it)->isScriptable())
				return *it._Ptr;
	}

	return NULL;
}


obj_type obj::getType() const
{
	return _ot;
}



ostream& obj::print(ostream&o) const
{
	module_container::const_iterator it;

	loader::printStart(o, "obj");
	loader::print(o, _ot, "_ot"); // pisanie obj_type
	for (it = _modules.begin(); it != _modules.end(); ++it)
		(*it)->print(o);
	loader::printEnd(o);
	return o;
}


istream& obj::scan(istream& i, const std::string filename)
{
	obj_type ot;
	module_container::const_iterator it;

	loader::scanStart(i, filename);

	// sprawdzanie poprawnosci ot obiektu z plikiem
	loader::scan(i, ot, filename);
	if (_ot != ot)
		throw wexception_t(G::text("unamatchedOT"), filename);

	// skanowanie wszystkich modulow
	for (it = _modules.begin(); it != _modules.end(); ++it)
		(*it)->scan(i, filename);

	loader::scanEnd(i);
	return i;
}


ostream& obj::operator<<(ostream& o) const
{
	return print(o);
}
istream& obj::operator>>(istream& i)
{
	return scan(i);
}
std::string obj::toString() const
{
	stringstream ss;
	print(ss);
	return ss.str();
}




////////////////
///// TASK /////
////////////////

void task_o_set_position::exec()
{
	module* temp;

	if (temp = _dst->getModule(module_type::MODULE_DRAWABLE))
		static_cast<m_drawable*>(temp)->position(_point);

	if (temp = _dst->getModule(module_type::MODULE_PHYSICAL))
		static_cast<m_physical*>(temp)->position(_point);

	if (temp = _dst->getModule(module_type::MODULE_SCRIPTABLE))
		static_cast<m_scriptable*>(temp)->position(_point);
}


/*void task_o_checkCollision::exec()
{
	m_physical* mp = static_cast<m_physical*>(_dst->getModule(module_type::MODULE_PHYSICAL));

	mp->checkCollision(_obj);
}*/