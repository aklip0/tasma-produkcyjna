#pragma once
#include <string>
#include <sstream>
#include <exception>

using namespace std;

/*template<typename T>
T min(const T t1, const T t2)
{
	return t1 > t2 ? t2 : t1;
}


template<typename T>
T max(const T t1, const T t2)
{
	return t1 < t2 ? t2 : t1;
}*/


template<typename T>
T abs(const T t)
{
	return t > 0 ? t : -t;
}


template <typename T>
short sgn(T val)
{
	return (T(0) < val) - (val < T(0));
}


template<typename T>
T* new_instanceS(T* old)
{
	return new T;
}

// variadic pattern with elipsis symbol (...)
template<typename T, typename... args>
T* new_instance(T* pattern, args... parameters)
{
	return new T(parameters...);
}

template<typename T> class size;
template<typename T = int> class point;
template<typename T = int> class rectangle;

//////////////////////////////
////////   POINT   ///////////
//////////////////////////////

template<class T>
class point
{
public:

	point()										: x(0), y(0) {}
	point(T D)									: x(D), y(D) {}
	point(T X, T Y)								: x(X), y(Y) {}
	point(const point& p)						: x(p.x), y(p.y) {}

	//template<typename U>
	point(size<T>& s)						: x(s.width), y(s.height) {}


	point operator+(const point& p)
	{
		return point(p.x + x, p.y + y);
	}
	point& operator+=(const point& p)
	{
		x = p.x + x;
		y = p.y + y;
		return *this;
	};
	point operator*(const T s);
	void move(T rx, T ry)
	{
		x += rx;
		y += ry;
	}
	void move(double a, T r)
	{
		x += (cos(a) * r);
		y -= (sin(a) * r);
	}
	void setPosition(T X, T Y)
	{
		x = X; y = Y;
	}

	friend ostream& operator<< <>(ostream& o, const point<T>& p);
	friend istream& operator>> <>(istream& o, point<T>& p);

	T x, y;
};

typedef point<int> vector2I;
typedef point<float> vector2F;
typedef point<double> vector2D;

typedef point<int> pointI;
typedef point<unsigned int> pointUI;
typedef point<float> pointF;

//////////////////////////////
////////   SIZE   ////////////
//////////////////////////////

template<typename T=int>
class size
{
public:
	size() : width(0), height(0)								{};
	size(T w, T h) : width(w), height(h)						{};

	T width, height;

	void scale(float s);//											{width *= s; height *= s; };
	size<T> operator+(const size<T>&);
	size<T>& operator+=(const size<T>&);
	point<T> operator();

	friend ostream& operator<< <>(ostream& o, const size<T>& s);
	friend istream& operator>> <>(istream& o, size<T>& s);

	std::string toString();
};


typedef size<int> sizeI;
typedef size<float> sizeF;
typedef size<double> sizeD;

//////////////////////////////
////////   RECTANGLE   ///////
//////////////////////////////

// T1 - position
template<typename T1>
class rectangle
{
private:
	T1 l_X, r_X;
	T1 t_Y, b_Y;

public:
	rectangle() : l_X(0), r_X(0), t_Y(0), b_Y(0) {}
	rectangle(const point<T1>& p1, const point<T1>& p2)
	{
		l_X = min(p1.X, p2.X);
		r_X = max(p1.X, p2.X);
		t_Y = min(p1.Y, p2.Y);
		b_Y = max(p1.Y, p2.Y);
	}
	rectangle(const point<T1>& p, const size<T1>& s) : l_X(p.X), t_Y(p.Y)
	{
		r_X = l_X + s.width;
		b_Y = t_Y + s.height;
	}
	rectangle(T1 x, T1 y, T1 w, T1 h) : l_X(x), r_X(x + w), t_Y(y), b_Y(y + h) {}

	//pointI operator=(const pointI&);
	point<T1> operator+(const point<T1>& pp)
	{
		point<T1> pp;
		pp.X = p.X + l_X;
		pp.Y = p.Y + t_Y;
		return pp;
	}

	T1 width() const { return (r_X - l_X); }
	void width(T1 w) { r_X = l_X + w; }
	T1 height() const { return b_Y - t_Y; }
	void height(T1 h) { b_Y = t_Y + h; }

	T1 X() const { return l_X; }
	void X(T1 x) { this->left(x); }
	T1 Y() const { return t_Y; }
	void Y(T1 y) { this->top(y); }

	T1 right() const { return r_X; }
	void right(T1 x)
	{
		T1 w = width();
		r_X = x;
		l_X = x - w;
	}
	T1 left() const { return l_X; }
	void left(T1 x)
	{
		T1 w = width();
		l_X = x;
		r_X = l_X + w;
	}

	T1 top() const { return t_Y; }
	void top(T1 y)
	{
		T1 h = height();
		t_Y = y;
		b_Y = y + h;
	}
	T1 bottom() const { return b_Y; }
	void bottom(T1 y)
	{
		T1 h = height();
		b_Y = y;
		t_Y = y - h;
	}

	void move(T1 x, T1 y)
	{
		left(l_X + x);
		top(t_Y + y);
	}
	point<T1> position() const
	{
		return point<T1>(left(), top());
	}
	void position(const point<T1> &p)
	{
		T1 t = width();
		l_X = p.x;
		r_X = p.x + t;

		t = height();
		t_Y = p.y;
		b_Y = p.y + t;
	}

	size<T1> Size() const { return size<T1>(width(), height()); }
	void Size(T1 w, T1 h) { width(w); height(h); }
	void Size(const size<T1>& siz) { width(siz.width); height(siz.height); }

	void scale(double s)
	{
		double d = static_cast<double>(width());
		r_X = l_X + static_cast<T1>(d * s);

		d = static_cast<double>(height());
		b_Y = t_Y + static_cast<T1>(d * s);
	}
	point<T1> middle() const
	{
		point<T1> p;
		p.x = (r_X + l_X) / 2;
		p.y = (t_Y + b_Y) / 2;
		return p;
	}

	T1 distance(const rectangle& rec) const;
	size<T1> intersects(const rectangle& r);
};

typedef rectangle<int> rectangleI;
typedef rectangle<float> rectangleF;



template<typename T, typename U=T>
class circle
{
public:
	circle() : r(0) {}
	circle(point<T> PT, U R) : pt(PT), r(R) {}
	circle(T X, T Y, U R) : pt(X, Y), r(R) {}

	point<T> pt;
	U r;
};

typedef circle<float> circleF;
typedef circle<int> circleI;

//////////////////////////////
////////   FUNCTORS    ///////
//////////////////////////////
template<typename T=int> // to nie musi byc szablon, ale nie chcialem robic nowego .cpp tylko dla tej funkcji
void throw_parsing_error(std::string t_name, streampos pos, int codeline = 0)
{
	std::string err("Error with ");
	err += t_name;
	err += " pair parsing, file pos: ";
	err += to_string(pos);
	err += "  ";
	err += __FILE__;
	err += ":" + to_string(codeline);
	throw exception(err.c_str());
}


//template<typename T, template<class...> class C, class... Args>
//istream& loadPair(istream& i, C<T>& w1, C<T>& w2)
template<typename T>
istream& loadPair(istream& i, T& w1, T& w2)
{
	try
	{
		char ch;
		i >> ch;

		if (ch != '(')
			throw_parsing_error(typeid(w1).name(), i.tellg(), __LINE__);

		i >> w1;
		i >> ch;

		if (ch != ';' && ch!= ',')
			throw_parsing_error(typeid(w1).name(), i.tellg(), __LINE__);

		i >> w2;
		i >> ch;

		if (ch != ')')
			throw_parsing_error(typeid(w1).name(), i.tellg(), __LINE__);

		if (i.fail())
			throw_parsing_error(typeid(w1).name(), i.tellg(), __LINE__);
	}
	catch (const std::bad_typeid&)
	{
		throw_parsing_error("NULL", i.tellg(), __LINE__);
	};

	return i;
}

template<typename T>
ostream& savePair(ostream& o, T& w1, T& w2)
{
	return o << "(" << w1 << ";" << w2 << ")";
}


/////////////////////////////////////////////////////////////////////////
////////// IMPLEMENTATION ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////


/// POINT
template<typename T>
point<T> point<T>::operator*(const T l)
{
	return point(x*l, y*l);
}

template<typename T>
ostream& operator<<(ostream& o, const point<T>& p)
{
	return savePair(o, p.x, p.y);
}

template<typename T>
istream& operator>>(istream& i, point<T>& p)
{
	return loadPair(i, p.x, p.y);
}


/// SIZE
template<typename T>
void size<T>::scale(float s)
{
	width *= s;
	height *= s;
}

template<typename T>
std::string size<T>::toString()
{
	std::sstream ss;
	ss << "size(w:" << width << " h:" << height << ")";
	return ss.str();
}

template<typename T>
size<T> size<T>::operator+(const size<T>& s)
{
	return size<T>(width + s.width, height + s.height);
}

template<typename T>
size<T>& size<T>::operator+=(const size<T>& s)
{
	width += s.width;
	height += s.height;
	return *this;
}

template<typename T>
istream& operator>>(istream& i, size<T>& s)
{
	return loadPair(i, s.width, s.height);
}

template<typename T>
ostream& operator<<(ostream& o, const size<T>& s)
{
	return savePair(o, s.width, s.height);
}



//////////////////////////////
////////   RECTANGLE   ///////
//////////////////////////////

template<typename T>
size<T> rectangle<T>::intersects(const rectangle& r)
{

	// spr czy jest w srodku
	if (r.left() >= right())
		return size<T>(0, 0);
	if (r.right() <= left())
		return size<T>(0, 0);
	if (r.top() >= bottom())
		return size<T>(0, 0);
	if (r.bottom() <= top())
		return size<T>(0, 0);

	//zmierz jak mocno sie nakladaja
	size<T> s;

	if (right() > r.left()) // z prawej
		s.width = right() - r.left(); // plus
	if (left() < r.right()) // z lewej 
	{
		if (abs(s.width) > abs(left() - r.right()))
			s.width = left() - r.right(); // minus
	}


	if (top() < r.bottom()) // z gory
		s.height = r.bottom() - top(); // plus
	if (bottom() > r.top()) // z dolu
	{
		if (abs(s.height) > abs(r.top() - bottom()))
			s.height = r.top() - bottom(); // minus
	}

	return s;
}

template <typename T>
T rectangle<T>::distance(const rectangle& rec) const
{
	point<T1> p1 = middle();
	point<T1> p2 = rec.middle();

	T1 x = p1.X - p2.X;
	T2 y = p1.Y - p2.Y;

	return static_cast<T1>(sqrt(static_cast<float>(x*x + y*y)));
}